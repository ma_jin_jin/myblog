function slideshow(){
    $('#index-list li').each(function(){
        $(this).css('opacity','0.5');
        if($(this).attr('index') == 1){
            $(this).css('opacity','1');
        }
        $(this).click(function(){
            var index = $(this).attr('index');
            var img = "picture" + index;
            $("#img-list img").attr('src','../img/'+img+'.jpg');
            $("#img-list img").attr('imgIndex',index);
            $(this).css('opacity','1');
            $(this).siblings().css('opacity','0.5');
        });
    });
    $('.toLeft').click(function () {
        var index = $("#img-list img").attr('imgIndex');
        if (index > 1 ){
            index--;
            var img = "picture" + index;
            $("#img-list img").attr('src','../img/'+img+'.jpg');
            $("#img-list img").attr('imgIndex',index);
            $('#index-list li:eq('+(index-1)+')').css('opacity','1');
            $('#index-list li:eq('+(index-1)+')').siblings().css('opacity','0.5');
        }
    });
    $('.toRight').click(function () {
        var index = $("#img-list img").attr('imgIndex');
        if (index < $('#index-list li').length ){
            index++;
            var img = "picture" + index;
            $("#img-list img").attr('src','../img/'+img+'.jpg');
            $("#img-list img").attr('imgIndex',index);
            $('#index-list li:eq('+(index-1)+')').css('opacity','1');
            $('#index-list li:eq('+(index-1)+')').siblings().css('opacity','0.5');
        }
    });
    var direction = 1; //向右
    function run() {
        var index = $("#img-list img").attr('imgIndex');
        if(index == $('#index-list li').length){
            direction = 0; //向左
        }
        if (index == 1){
            direction = 1;//向右
        }

        if (direction == 1){
            index++;
            var img = "picture" + index;
            $("#img-list img").attr('src','../img/'+img+'.jpg');
            $("#img-list img").attr('imgIndex',index);
            $('#index-list li:eq('+(index-1)+')').css('opacity','1');
            $('#index-list li:eq('+(index-1)+')').siblings().css('opacity','0.5');
        }

        if (direction == 0){
            index--;
            var img = "picture" + index;
            $("#img-list img").attr('src','../img/'+img+'.jpg');
            $("#img-list img").attr('imgIndex',index);
            $('#index-list li:eq('+(index-1)+')').css('opacity','1');
            $('#index-list li:eq('+(index-1)+')').siblings().css('opacity','0.5');
        }
    }
    var timer = setInterval(run,3000);
    $('.slideshow').mouseenter(function () {
        clearInterval(timer);
    });
    $('.slideshow').mouseleave(function () {
        timer = setInterval(run,3000);
    })



}
function recommend() {

    $('.recommend-nav a').each(function () {
        $(this).click(function () {
            $(this).css('border-bottom','2px solid black');
            $(this).siblings().css('border-bottom','none');
            if($(this).attr('index') == 1){
                $('#panel1').show();
                $('#panel2').hide();
            }else {
                $('#panel2').show();
                $('#panel1').hide();
            }
        });
    });
    $('.recommend-nav a:eq(0)').trigger('click');
}
function newblog() {

    $('.news-blog-item').each(function () {
        $(this).mouseenter(function () {
            $(this).find('p').css('color','black');
        });
        $(this).mouseleave(function () {
            $(this).find('p').css('color','#7b7575');
        });
    });

    $('.first').mouseenter(function () {
        $(this).find('h4').css('font-weight','bold');
    });
    $('.first').mouseleave(function () {
        $(this).find('h4').css('font-weight','inherit');
    });
}